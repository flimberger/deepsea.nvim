local p = require('deepsea').palette
return {
	normal = {
		a = { bg = p.mediumblue, fg = p.lightblue, },
		b = { bg = p.darkblue, fg = p.lightblue, },
		c = { bg = p.mediumblue, fg = p.lightgray, }
	},
	insert = {
		a = { bg = p.green, fg = p.darkerblue, },
	},
	visual = {
		a = { bg = p.lightblue, fg = p.darkerblue, },
	},
	replace = {
		a = { bg = p.orange, fg = p.darkerblue, },
	},
	command = {
		a = { bg = p.yellow, fg = p.darkerblue, },
	},
	inactive = {
		a = { bg = p.mediumgray, fg = p.darkerblue, },
	},
}
