local M = {}

-- colour palette
local p = {
	cyan = '#33CCCC',
	darkblue = '#002233',
	darkerblue = '#011220',
	green = '#00CC00',
	lightblue = '#6699BB',
	lightgray = '#AAAAAA',
	mediumblue = '#123456',
	mediumgray = '#888888',
	orange = '#FF9900',
	red = '#FF0000',
	yellow = '#FFCC00',
	white = '#FFFFFF',
}
-- export the palette so it can be used by the lualine theme
M.palette = p

-- theme
local t = {
	ColorColumn = { bg = p.darkerblue, fg = p.orange },
	CursorLine = { bg = p.mediumblue },
	CursorLineNr = { bg = p.mediumblue, fg = p.red },
	DiagnosticError = { fg = p.red },
	DiagnosticWarn = { fg = p.orange },
	DiagnosticInfo = { fg = p.lightgrey },
	DiagnosticHint = { fg = p.lightblue },
	DiagnosticSignError = { bg = p.darkerblue, fg = p.red },
	DiagnosticSignWarn = { bg = p.darkerblue, fg = p.orange },
	DiagnosticSignInfo = { bg = p.darkerblue, fg = p.lightgrey },
	DiagnosticSignHint = { bg = p.darkerblue, fg = p.lightblue },
	DiffAdd = { fg = p.green },
	DiffChange = { fg = p.yellow },
	DiffDelete = { fg = p.orange },
	DiffText = { fg = p.lightgrey },
	ErrorMsg = { bg = p.orange, fg = p.darkerblue },
	FoldColumn = { bg = p.darkerblue },
	IncSearch = { bg = p.lightblue, fg = p.white },
	LineNr = { bg = p.darkerblue, fg = p.yellow },
	MatchParen = { fg = p.yellow },
	NonText = { fg = p.lightblue },
	Normal = { bg = p.darkblue, fg = p.lightgray },
	NormalFloat = { bg = p.mediumblue, fg = p.lightgray },
	NormalNC = { bg = p.darkblue, fg = p.lightgray },
	Pmenu = { bg = p.darkerblue, fg = p.lightblue },
	PmenuSbar = { bg = p.lightblue },
	PmenuSel = { bg = p.mediumblue, fg = p.yellow },
	PmenuThumb = { bg = p.lightgrey },
	Question = { fg = p.yellow },
	Search = { bg = p.lightblue, fg = p.white },
	SignColumn = { bg = p.darkerblue },
	SpecialKey = { fg = p.orange },
	StatusLine = { bg = p.mediumblue },
	StatusLineNC = { bg = p.darkblue },
	TabLine = { bg = p.darkerblue, fg = p.lightgray },
	TabLineFill = { bg = p.darkerblue, fg = p.lightgray },
	TabLineSel = { bg = p.darkblue, fg = p.yellow },
	TermCursor = { bg = p.yellow },
	TermCursorNC = { bg = p.yellow },
	Title = { bg = darkerblue, fg = p.cyan },
	VertSplit = { bg = p.darkblue, fg = p.lightblue },
	Visual = { bg = p.lightblue },
	WarningMsg = { fg = p.yellow },

	-- syntax
	Boolean = { fg = p.cyan },
	Character = { fg = p.lightblue },
	Comment = { fg = p.mediumgray },
	Constant = { fg = p.yellow },
	Debug = { fg = p.orange },
	Delimiter = { fg = p.lightgray },
	Error = { bg = p.red, fg = p.white },
	Function = { fg = p.cyan },
	Identifier = { fg = p.lightgray },
	Ignore = { fg = p.mediumgray },
	Include = { fg = p.cyan },
	PreProc = { fg = p.green },
	Special = { fg = p.yellow },
	SpecialComment = { fg = p.green },
	Statement = { fg = p.cyan },
	String = { fg = p.lightblue },
	Todo = { bg = p.yellow, fg = p.darkblue },
	Type = { fg = p.cyan },
	Typedef = { fg = p.cyan },

	-- markup
	htmlBold = { fg = p.lightblue, bold = true },
	htmlItalic = { fg = p.lightblue, italic = true },

	markdownDelimiter = { fg = p.cyan },
	markdownH1 = { fg = p.lightblue },
	markdownH1Delimiter = { fg = p.cyan },
	markdownH2 = { fg = p.lightblue },
	markdownH2Delimiter = { fg = p.cyan },
	markdownH3 = { fg = p.lightblue },
	markdownH3Delimiter = { fg = p.cyan },
	markdownH4 = { fg = p.lightblue },
	markdownH4Delimiter = { fg = p.cyan },
	markdownH5 = { fg = p.lightblue },
	markdownH5Delimiter = { fg = p.cyan },
	markdownH6 = { fg = p.lightblue },
	markdownH6Delimiter = { fg = p.cyan },
	markdownLinkText = { underline = true },
	markdownUrl = { link = 'markdownLinkText' },

        yamlBlockCollectionItemStart = { link = 'Normal' },
        yamlBlockMappingKey = { fg = p.cyan },
        yamlFlowIndicator = { link = 'Normal' },
        yamlKeyValueDelimiter = { link = 'Normal' },
}

function M.colorscheme()
	if vim.g.colors_name then
		vim.cmd('hi clear')
	end

	vim.g.colors_name = 'deepsea'
	for group, style in pairs(t) do
		vim.api.nvim_set_hl(0, group, style)
	end
end

return M
