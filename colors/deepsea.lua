vim.cmd 'packadd deepsea'
if vim.g.colors_debug then
  package.loaded['deepsea'] = nil
end
require('deepsea').colorscheme()
